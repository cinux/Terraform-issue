terraform {
  backend "local" { path = "/home/cinux/Dokumente/terraform/tf_files/terraform.tfstate" }
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.11"
    }
    openwrt = {
      source  = "joneshf/openwrt"
      version = "0.0.20"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://${var.proxmox_cluster_node}:8006/api2/json"
}


module "proxmox_vm" {
  #source                   = "../terraform-module"
  for_each                 = { for k, v in var.vms : k => v }
  proxmox_vm_name          = each.value.vm_name
  proxmox_vm_target_node   = each.value.vm_details.target_node
  proxmox_vm_full_clone    = each.value.vm_details.full_clone
  proxmox_vm_clone         = each.value.vm_details.clone
  proxmox_vm_resource_pool = each.value.vm_details.resource_pool
  proxmox_vm_desc          = each.value.vm_details.desc
  proxmox_vm_qemu_os       = each.value.vm_details.qemu_os

  proxmox_vm_cores   = each.value.vm_details.cores
  proxmox_vm_sockets = each.value.vm_details.sockets
  proxmox_vm_vcpus   = each.value.vm_details.vcpus
  proxmox_vm_memory  = each.value.vm_details.memory
  proxmox_vm_balloon = each.value.vm_details.balloon

  proxmox_vm_scsihw = each.value.vm_details.scsihw

  proxmox_vm_onboot  = each.value.vm_details.onboot
  proxmox_vm_boot    = each.value.vm_details.boot
  proxmox_vm_agent   = each.value.vm_details.agent
  proxmox_vm_cpu     = each.value.vm_details.cpu
  proxmox_vm_numa    = each.value.vm_details.numa
  proxmox_vm_hotplug = each.value.vm_details.hotplug
  proxmox_vm_os_type = each.value.vm_details.os_type

  proxmox_vm_ciuser    = each.value.vm_details.ciuser
  proxmox_vm_sshkeys   = each.value.vm_details.sshkeys
  proxmox_vm_ipconfig0 = each.value.vm_details.ipconfig0
}
