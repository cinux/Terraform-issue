variable "proxmox_vm_name" {
  description = "Name of the VM."
  type        = string
}
variable "proxmox_vm_target_node" {
  description = "Name of the node where VM deployed."
  type        = string
}
variable "proxmox_vm_full_clone" {
  description = "Flag to create VM as full clone or as linked VM."
  type        = bool
}
variable "proxmox_vm_clone" {
  description = "Name of the template to clone."
  type        = string
}
variable "proxmox_vm_resource_pool" {
  description = "Name of the resource pool to be deployed to."
  type        = string
}
variable "proxmox_vm_desc" {
  description = "Description of the VM."
  type        = string
}
variable "proxmox_vm_qemu_os" {
  description = "The type of OS in the guest."
  type        = string
}
variable "proxmox_vm_cores" {
  description = "Number of the cores."
  type        = number
}
variable "proxmox_vm_sockets" {
  description = "Number of sockets."
  type        = number
}
variable "proxmox_vm_vcpus" {
  description = "Number of vCPUs."
  type        = number
}
variable "proxmox_vm_memory" {
  description = "Number of Memory."
  type        = number
}
variable "proxmox_vm_balloon" {
  description = "The minimum amount of memory to allocate to the VM in Megabytes."
  type        = number
}
variable "proxmox_vm_scsihw" {
  description = "The SCSI controller to emulate"
  type        = string
}
variable "proxmox_vm_onboot" {
  description = "Whether to have the VM startup after the PVE node starts."
  type        = bool
}
variable "proxmox_vm_boot" {
  description = "The boot order for the VM."
  type        = string
}
variable "proxmox_vm_agent" {
  description = "Set qemu guest agent"
  type        = number
}
variable "proxmox_vm_cpu" {
  description = "The type of CPU to emulate in the Guest."
  type        = string
}
variable "proxmox_vm_numa" {
  description = "Set Non-Uniform Memory Access in the guest."
  type        = bool
}
variable "proxmox_vm_hotplug" {
  description = "Comma delimited list of hotplug features to enable."
  type        = string
}
variable "proxmox_vm_os_type" {
  description = "Which provisioning method to use, based on the OS type."
  type        = string
}
variable "proxmox_vm_ipconfig0" {
  description = "The first IP address to assign to the guest."
  type        = string
  default     = "ip=dhcp"
}

# Cloud-init

variable "proxmox_vm_ciuser" {
  description = "The user rolled out via cloud-init."
  type        = string
  default     = "itadmin"
}

variable "proxmox_vm_sshkeys" {
  description = "SSH Public key for itadmin user."
  type        = string
  default     = ""
}
