terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      # need to pin to version 2.9.11 because of:
      # https://github.com/Telmate/terraform-provider-proxmox/issues/762
      version = "2.9.11"
    }
  }
}

resource "proxmox_vm_qemu" "vm" {
  provisioner "local-exec" {
    working_dir = ".ansible/"
    command     = "make deps && export ANSIBLE_HOST_KEY_CHECKING=false; export ANSIBLE_ROLES_PATH=roles; ansible-playbook -u cinux -i inventory/inventory --limit \"${var.proxmox_vm_name}\" all.yml"
  }

  name        = var.proxmox_vm_name
  target_node = var.proxmox_vm_target_node
  full_clone  = var.proxmox_vm_full_clone
  clone       = var.proxmox_vm_clone
  pool        = var.proxmox_vm_resource_pool
  desc        = var.proxmox_vm_desc
  qemu_os     = var.proxmox_vm_qemu_os

  cores   = var.proxmox_vm_cores
  sockets = var.proxmox_vm_sockets
  vcpus   = var.proxmox_vm_vcpus
  memory  = var.proxmox_vm_memory
  balloon = var.proxmox_vm_balloon

  scsihw = var.proxmox_vm_scsihw

  onboot  = var.proxmox_vm_onboot
  boot    = var.proxmox_vm_boot
  agent   = var.proxmox_vm_agent
  cpu     = var.proxmox_vm_cpu
  numa    = var.proxmox_vm_numa
  hotplug = var.proxmox_vm_hotplug
  os_type = var.proxmox_vm_os_type

  # Cloud-init
  ciuser    = var.proxmox_vm_ciuser
  sshkeys   = var.proxmox_vm_sshkeys
  ipconfig0 = var.proxmox_vm_ipconfig0

  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "4M"
    slot = 0
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "40G"
    slot = 1
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "10G"
    slot = 2
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "20G"
    slot = 3
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "20G"
    slot = 4
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "20G"
    slot = 5
  }
  disk {
    type = "virtio"
    storage = "local-lvm"
    size = "20G"
    slot = 6
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
  }
}
