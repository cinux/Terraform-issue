proxmox_cluster_node = "pve"
vms = [
  { vm_details = {
    sockets       = 1
    cores         = 12,
    memory        = 2048,
    target_node   = "pve",
    full_clone    = false,
    clone         = "tmpl-debian-12.0-v0.0.2",
    resource_pool = "Testing"
    desc          = "used for Paperless-ngx",
    qemu_os       = "other",
    vcpus         = 2,
    balloon       = 2048,
    cpu           = "kvm64",
    numa          = true,
    hotplug       = "network,disk,cpu,memory"
    #
    scsihw    = "virtio-scsi-pci"
    onboot    = "true"
    boot      = "order=virtio1;ide0;net0"
    agent     = 1
    os_type   = "cloud-init"
    ip        = "10.0.0.12"
    ipconfig0 = "ip=10.0.0.12/24,gw=10.0.0.1"
    ciuser    = "cinux"
    sshkeys   = <<EOF
ssh-rsa xxx
ssh-ed25519 xxx
EOF
    },
    vm_name = "paperless"
  },
  { vm_details = {
    sockets       = 1
    cores         = 12,
    memory        = 6144,
    target_node   = "pve",
    full_clone    = false,
    clone         = "tmpl-debian-12.0-v0.0.2",
    resource_pool = "Testing"
    desc          = "used for immich",
    qemu_os       = "other",
    vcpus         = 4,
    balloon       = 6144,
    cpu           = "kvm64",
    numa          = true,
    hotplug       = "network,disk,cpu,memory"
    #
    scsihw    = "virtio-scsi-pci"
    onboot    = "true"
    boot      = "order=virtio1;ide0;net0"
    agent     = 1
    os_type   = "cloud-init"
    ip        = "10.0.0.13"
    ipconfig0 = "ip=10.0.0.13/24,gw=10.0.0.1"
    ciuser    = "cinux"
    sshkeys   = <<EOF
ssh-rsa xxx
ssh-ed25519 xxx
EOF
    },
    vm_name = "immich"
  },
  { vm_details = {
    sockets       = 1
    cores         = 12,
    memory        = 6144,
    target_node   = "pve",
    full_clone    = false,
    clone         = "tmpl-debian-12.0-v0.0.2",
    resource_pool = "Testing"
    desc          = "used for photoprism",
    qemu_os       = "other",
    vcpus         = 4,
    balloon       = 6144,
    cpu           = "kvm64",
    numa          = true,
    hotplug       = "network,disk,cpu,memory"
    #
    scsihw    = "virtio-scsi-pci"
    onboot    = "true"
    boot      = "order=virtio1;ide0;net0"
    agent     = 1
    os_type   = "cloud-init"
    ip        = "10.0.0.14"
    ipconfig0 = "ip=10.0.0.14/24,gw=10.0.0.1"
    ciuser    = "cinux"
    sshkeys   = <<EOF
ssh-rsa xxx
ssh-ed25519 xxx
EOF
    },
    vm_name = "photoprism"
  },
]
