variable "proxmox_cluster_node" {
  description = "FQDN of Proxmox Cluster node"
  type = string
}
variable "vms" {
  type = list(
    object({
      vm_name = string,
      vm_details = object({
        sockets       = number,
        cores         = number,
        memory        = number,
        target_node   = string,
        full_clone    = bool,
        clone         = string,
        resource_pool = string,
        desc          = string,
        qemu_os       = string
        vcpus         = number
        balloon       = number
        onboot        = bool
        scsihw        = string
        boot          = string
        agent         = number
        cpu           = string
        numa          = bool
        hotplug       = string
        os_type       = string
	ip            = string
        ipconfig0     = string
        sshkeys       = string
        ciuser        = string
      })
    })
  )
}
